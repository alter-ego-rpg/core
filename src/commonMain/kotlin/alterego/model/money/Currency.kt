package alterego.model.money
import alterego.model.entity.Described

data class Currency(val name: String, override var description: String, val symbol: Char): Described {
    val unit: Money = Money(1.0, this)

    override fun toString(): String {
        return "${this.name} (${this.symbol})"
    }
}

open class Money(value: Number, currency: Currency): Comparable<Money> {
    open val value: Double = value.toDouble()
    open val currency: Currency = currency

    protected open fun <M: Money> make(value: Number, currency: Currency): M = Money(value, currency) as M

    override fun equals(other: Any?): Boolean {
        if (other is Money)
            return this.value == other.value && this.currency == other.currency
        return false
    }

    override fun hashCode(): Int {
        return this.value.hashCode() * this.currency.hashCode() * Money::class.hashCode()
    }

    internal fun validateCurrency(other: Currency) {
        if (this.currency != other)
            throw IncompatibleCurrencyException(this.currency, other)
    }

    internal fun validateCurrency(other: Money) {
        validateCurrency(other.currency)
    }

    override operator fun compareTo(other: Money): Int {
        validateCurrency(other)

        return this.value.compareTo(other.value)
    }

    operator fun <M: Money> plus(other: Money): M {
        validateCurrency(other)

        return make(this.value + other.value, this.currency)
    }

    operator fun <M: Money> plus(other: Number): M = make(this.value + other.toDouble(), this.currency)

    operator fun <M: Money> minus(other: Money): M {
        validateCurrency(other)

        return make(this.value - other.value, this.currency)
    }

    operator fun <M: Money> minus(other: Number): M = make(this.value - other.toDouble(), this.currency)

    operator fun <M: Money> times(scale: Number): M = make(this.value * scale.toDouble(), this.currency)

    operator fun <M: Money> div(divisor: Number): M = make(this.value / divisor.toDouble(), this.currency)

    operator fun unaryMinus(): Money = make(-this.value, this.currency)

    operator fun rangeTo(other: Money): ClosedRange<Money> = MoneyClosedRange(this, other)

    operator fun rangeTo(other: Number): ClosedRange<Money> = MoneyClosedRange(this, other.toDouble())

    override fun toString(): String {
        return "Money(value=$value, currency=$currency)"
    }
}

operator fun Number.plus(money: Money): Money = Money(this.toDouble() + money.value, money.currency)

operator fun Number.minus(money: Money): Money = Money(this.toDouble() - money.value, money.currency)

operator fun Number.times(money: Money): Money = Money(this.toDouble() * money.value, money.currency)

operator fun Number.rangeTo(other: Money): ClosedRange<Money> = MoneyClosedRange(this, other)


open class FlexMoney(value: Number, currency: Currency): Money(value, currency) {
    override var value: Double = value.toDouble()
    override val currency: Currency = currency

    override fun <M: Money> make(value: Number, currency: Currency): M = FlexMoney(value, currency) as M

    operator fun plusAssign(other: Money) {
        validateCurrency(other)

        this.value += other.value
    }

    operator fun plusAssign(other: Number) {
        this.value += other.toDouble()
    }

    operator fun minusAssign(other: Money) {
        validateCurrency(other)

        this.value -= other.value
    }

    operator fun minusAssign(other: Number) {
        this.value -= other.toDouble()
    }

    operator fun timesAssign(scale: Number) {
        this.value *= scale.toDouble()
    }

    operator fun divAssign(divisor: Number) {
        this.value /= divisor.toDouble()
    }

    override fun toString(): String {
        return "FlexMoney(value=$value, currency=$currency)"
    }

}

operator fun Number.plus(money: FlexMoney): FlexMoney = FlexMoney(this.toDouble() + money.value, money.currency)

operator fun Number.minus(money: FlexMoney): FlexMoney = FlexMoney(this.toDouble() - money.value, money.currency)

operator fun Number.times(money: FlexMoney): FlexMoney = FlexMoney(this.toDouble() * money.value, money.currency)


class MoneyClosedRange : ClosedRange<Money> {

    /* Works with any subclass of Money, but guarantees that the bounds are immutable
    * as the Money base class is designed to be immutable. */

    override val start: Money by lazy { Money(startValue, currency) }
    override val endInclusive: Money by lazy { Money(endValue, currency) }

    private val startValue: Double
    private val endValue: Double
    private val currency: Currency

    constructor(rangeStart: Money, rangeEndInclusive: Money) {
        rangeStart.validateCurrency(rangeEndInclusive)

        this.currency = rangeStart.currency
        this.startValue = rangeStart.value
        this.endValue = rangeEndInclusive.value
    }

    constructor(rangeStart: Money, rangeEndInclusive: Number) {
        this.currency = rangeStart.currency
        this.startValue = rangeStart.value
        this.endValue = rangeEndInclusive.toDouble()
    }

    constructor(rangeStart: Number, rangeEndInclusive: Money) {
        this.currency = rangeEndInclusive.currency
        this.startValue = rangeStart.toDouble()
        this.endValue = rangeEndInclusive.value
    }

    constructor(start: Number, end: Number, currency: Currency) {
        this.startValue = start.toDouble()
        this.endValue = end.toDouble()
        this.currency = currency
    }

    override operator fun contains(value: Money): Boolean {
        value.validateCurrency(currency)

        return value.value in startValue..endValue
    }

    override fun isEmpty(): Boolean {
        return endValue < startValue
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as MoneyClosedRange

        if (currency != other.currency) return false
        if (startValue != other.startValue) return false
        return endValue == other.endValue
    }

    override fun hashCode(): Int {
        var result = currency.hashCode()
        result = 31 * result + startValue.hashCode()
        result = 31 * result + endValue.hashCode()
        return result
    }

    override fun toString(): String {
        return "MoneyClosedRange(startValue=$startValue, endValue=$endValue, currency=$currency)"
    }
}

class IncompatibleCurrencyException(val c1: Currency, val c2: Currency): Exception()
