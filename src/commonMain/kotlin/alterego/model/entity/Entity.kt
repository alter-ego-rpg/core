package alterego.model.entity


abstract class Entity: Described {
    abstract val id: String
    abstract var name: String
}

interface Described {
    var description: String
}