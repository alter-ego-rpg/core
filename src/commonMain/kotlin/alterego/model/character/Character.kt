package alterego.model.character
import alterego.model.entity.Described
import alterego.model.entity.Entity

/**
 * Abstract base class for characters.
 */
abstract class Character : Entity() {
    abstract val actions: MutableList<Actionable>
    abstract val enhancements: MutableList<Enhancement<Character>>
}

/**
 * Protocol for actions.
 */
interface Actionable: Described {
    /**
     * Executes the action.
     *
     * @param doer: The character performing the action.
     * @param targets: Collection of targets upon which the action is performed.
     */
    fun doAction(doer: Character, vararg targets: Entity)
}

/**
 * Interface for enhancements.
 */
interface Enhancement<T: Entity>: Described {
    /**
     * The entity 
     */
    val enhanced: T
    var enabled: Boolean
}