package alterego.util

const val ZERO: Byte = 0

operator fun Number.plus(other: Number): Number {
    var result = this
    when (this) {
        is Byte -> result = result as Byte + other
        is Short -> result = result as Short + other
        is Int -> result = result as Int + other
        is Long -> result = result as Long + other
        is Float -> result = result as Float + other
        is Double -> result = result as Double + other
    }
    return result
}
