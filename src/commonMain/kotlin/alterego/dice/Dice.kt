package alterego.dice
import kotlin.random.Random

/**
 * Mapping from a value to it's corresponding DieFace.
 */
typealias DieFaceMap<T> = MutableMap<T, DieFace<T>>

/**
 * Face of a die.
 *
 * While typically the face is numerical, it need not be.
 *
 * @property value The value represented by this DieFace.
 */
class DieFace<T: Any> private constructor(val value: T) {

    /**
     * Cache instances of DiceFace so that multiple calls to DieFace(face_value) return the same instance.
     */
    companion object {
        private val faceCache: DieFaceMap<Any> = HashMap()

        /**
         * @param value The value of the DiceFace
         * @return An instance of DiceFace representing the value.
         */
        operator fun <T: Any> invoke(value: T): DieFace<T> {
            if (value !in faceCache) {
                faceCache[value] = DieFace(value)
            }
            @Suppress("UNCHECKED_CAST")
            return faceCache[value] as DieFace<T>
        }
    }
}

/**
 * Map of the number of times each face appears on a die.
 */
class DistributionMap<T: Any>
    (private val map: Map<DieFace<T>, Int>): Map<DieFace<T>, Int> by map {

    override fun get(key: DieFace<T>): Int {
        val value = map[key]
        return value ?: 0
    }

    /* Keeping these commented as reference of what override options are available.
    override val entries: Set<Map.Entry<DiceFace<T>, Int>>
        get() = map.entries

    override val keys: Set<DiceFace<T>>
        get() = map.keys

    override val size: Int
        get() = map.size

    override val values: Collection<Int>
        get() = map.values

    override fun containsKey(key: DiceFace<T>): Boolean {
        return map.containsKey(key)
    }

    override fun containsValue(value: Int): Boolean {
        return map.containsValue(value)
    }

    override fun isEmpty(): Boolean {
        return map.isEmpty()
    }
    */
}

/**
 * Die.
 *
 * @property faceCount Number of faces on this Die.
 * @property faces List of DiceFace instances.  Note, duplicate faces are allowed.
 * @property faceDistribution Map of the number of times each face appears on the die.
 */
abstract class Die<T: Any> {

    open val faceCount: Int
        get() = faces.size

    abstract val faces: List<DieFace<T>>

    val faceDistribution: Map<DieFace<T>, Int> by lazy {
        val hashMap: HashMap<DieFace<T>, Int> = HashMap()
        for (face in faces) {
            hashMap[face] = 1 + (hashMap[face] ?: 0)
        }
        DistributionMap(hashMap)
    }

    /**
     * Rolls the die and returns a random DiceFace.
     *
     * @param rng Random number generator
     * @return One of the faces of the die chosen randomly (according to the rng).
     */
    fun roll(rng: Random = Random): DieFace<T> {
        return faces.random(rng)
    }
}

/**
 * Die for which the faces are determined by an enumerated type.
 */
class EnumDie<T: Enum<T>>(
    override val faces: List<DieFace<T>>
    ): Die<T>() {

    companion object {
        /**
         *
         */
        inline fun <reified E: Enum<E>> create(): EnumDie<E> {
            val enums = enumValues<E>()
            val enumFaces = List(enums.size) { DieFace(enums[it]) }
            return EnumDie(enumFaces)
        }

        /**
         *
         */
        inline operator fun <reified E: Enum<E>> invoke(): EnumDie<E> = create()
    }
}