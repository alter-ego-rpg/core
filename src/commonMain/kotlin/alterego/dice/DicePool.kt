package alterego.dice

import kotlin.random.Random

/**
 * Collection of dice to be handled as a single unit.
 *
 * @property dice List of dice.
 */
class DicePool {
    val dice: MutableList<Die<*>> = mutableListOf()

    /**
     * Rolls the die and returns a random DiceFace.
     *
     * @param rng Random number generator
     * @return List of the faces of the dice chosen randomly (according to the rng).
     */
    fun roll(rng: Random = Random): List<DieFace<*>> {
        val rolls = ArrayList<DieFace<*>>(dice.size)
        for (die in dice) {
            val face = die.roll(rng)
            rolls.add(face)
        }

        return rolls
    }

    operator fun plusAssign(die: Die<*>){
        dice.add(die)
    }

    operator fun plusAssign(other: DicePool) {
        dice.addAll(other.dice)
    }

    operator fun plus(die: Die<*>): DicePool {
        val newDicePool = DicePool()
        newDicePool.dice.addAll(this.dice)
        newDicePool.dice.add(die)
        return newDicePool
    }

    operator fun plus(other: DicePool): DicePool {
        val newDicePool = DicePool()
        newDicePool.dice.addAll(this.dice)
        newDicePool.dice.addAll(other.dice)
        return newDicePool
    }

}

operator fun Die<*>.plus(dicePool: DicePool): DicePool {
    val newDicePool = DicePool()
    newDicePool.dice.add(this)
    newDicePool.dice.addAll(dicePool.dice)
    return newDicePool
}

operator fun Die<*>.plus(die: Die<*>): DicePool {
    val newDicePool = DicePool()
    newDicePool.dice.add(this)
    newDicePool.dice.add(die)
    return newDicePool
}

operator fun Int.invoke(die: Die<*>): DicePool {
    val newDicePool = DicePool()
    for (i in 1..this) {
        newDicePool.dice.add(die)
    }
    return newDicePool
}
