package alterego.dice

/**
 * Standard die where each face has a unique number from 1 to faceCount.
 */
open class NumberDie(override val faceCount: Int) : Die<Int>() {
    override val faces: List<DieFace<Int>> by lazy {
        List(faceCount) { DieFace(it + 1) }
    }
}

/** Standard D2 die. */
val D2: NumberDie = NumberDie(2)

/** Standard D4 die. */
val D4: NumberDie = NumberDie(4)

/** Standard D6 die. */
val D6: NumberDie = NumberDie(6)

/** Standard D8 die. */
val D8: NumberDie = NumberDie(8)

/** Standard D10 die. */
val D10: NumberDie = NumberDie(10)

/** Standard D12 die. */
val D12: NumberDie = NumberDie(12)

/** Standard D20 die. */
val D20: NumberDie = NumberDie(20)

/** Standard D100 die. */
val D100: NumberDie = NumberDie(100)

/** Percentile D10 die. */
val DP10: NumberDie = object : NumberDie(10) {
    override val faces: List<DieFace<Int>> by lazy {
        List(10) { DieFace(it * 10) }
    }
}

/**
 * Common naming for the two (flat) sides of a coin.
 */
enum class CoinSides {
    /** The side commonly-known as Heads */
    HEADS,

    /** The side commonly-known as Tails */
    TAILS
}

/** Standard coin implemented as a die. */
val COIN: EnumDie<CoinSides> = EnumDie.create()
