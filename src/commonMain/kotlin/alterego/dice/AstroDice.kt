package alterego.dice

/**
 * Astrological "planets" to use as faces on a 12-sided die.
 *
 * @param symbol Single character symbol used to represent the planet.
 */
enum class Planets(val symbol: Char) {
    /** The Sun. */
    SUN('☉'),

    /** The Moon. */
    MOON('☾'),

    /** The planet Mercury. */
    MERCURY('☿'),

    /** The planet Venus. */
    VENUS('♀'),

    /** The planet Mars. */
    MARS('♂'),

    /** The planet Jupiter. */
    JUPITER('♃'),

    /** The planet Saturn. */
    SATURN('♄'),

    /** The planet Uranus. */
    URANUS('♅'),

    /** The planet Neptune. */
    NEPTUNE('♆'),

    /** The planet Pluto. */
    PLUTO('♇'),

    /** The "planet" Chiron. */
    CHIRON('⚷'),

    /** The lunar Node. */
    NODE('☊')
}

/**
 * Zodiac signs of western astrology to use as faces of a 12-sided die.
 *
 * @param symbol Single character symbol used to represent the zodiac sign.
 */
enum class Zodiac(val symbol: Char) {
    /** The zodiac sign of Aries. */
    ARIES('♈'),

    /** The zodiac sign of Taurus. */
    TAURUS('♉'),

    /** The zodiac sign of Gemini. */
    GEMINI('♊'),

    /** The zodiac sign of Cancer. */
    CANCER('♋'),

    /** The zodiac sign of Leo. */
    LEO('♌'),

    /** The zodiac sign of Virgo. */
    VIRGO('♍'),

    /** The zodiac sign of Libra. */
    LIBRA('♎'),

    /** The zodiac sign of Scorpio. */
    SCORPIO('♏'),

    /** The zodiac sign of Sagittarius. */
    SAGITTARIUS('♐'),

    /** The zodiac sign of Capricorn. */
    CAPRICORN('♑'),

    /** The zodiac sign of Aquarius. */
    AQUARIUS('♒'),

    /** The zodiac sign of Pisces. */
    PISCES('♓')
}

/**
 * Special die with faces representing astrological "planets".
 */
val DPlanet: Die<Planets> = EnumDie()

/**
 * Special die with faces representing astrological zodiac signs.
 */
val DZodiac: Die<Zodiac> = EnumDie()
