package alterego.dice
import alterego.util.*

class RollFormula {

}

class RollResult {
    val faces: List<DieFace<*>> = listOf()

    fun sum(): Number {
        var result: Number = ZERO
        for (f in faces) {
            if (f.value is Number) {
                result += f.value
            }
        }
        return result
    }
}