package alterego_tests

import kotlin.test.*

import alterego.dice.*
import kotlin.random.Random

const val RNG_SEED: Int = 123456

class DiceTests {
    @Test
    fun testStandardD2() {
        val die = D2
        assertEquals(2, die.faceCount)
        assertContains(die.faces, DieFace(1))
        assertContains(die.faces, DieFace(2))
        assertEquals(1, die.faceDistribution[DieFace(1)])
        assertEquals(1, die.faceDistribution[DieFace(2)])
    }

    @Test
    fun testStandardD2RNG() {
        val die = D2
        val rng = Random(RNG_SEED)
        assertEquals(1, die.roll(rng).value)
        assertEquals(1, die.roll(rng).value)
        assertEquals(2, die.roll(rng).value)
    }

    @Test
    fun testStandardCOIN() {
        val die = COIN
        assertEquals(2, die.faceCount)
        assertContains(die.faces, DieFace(CoinSides.HEADS))
        assertContains(die.faces, DieFace(CoinSides.TAILS))
        assertEquals(1, die.faceDistribution[DieFace(CoinSides.HEADS)])
        assertEquals(1, die.faceDistribution[DieFace(CoinSides.TAILS)])
    }

    @Test
    fun testStandardD2COIN() {
        val die = COIN
        val rng = Random(RNG_SEED)
        assertEquals(CoinSides.HEADS, die.roll(rng).value)
        assertEquals(CoinSides.HEADS, die.roll(rng).value)
        assertEquals(CoinSides.TAILS, die.roll(rng).value)
    }

    @Test
    fun testStandardD20() {
        val die = D20
        assertEquals(20, die.faceCount)

        for (i in 1..20)
            assertContains(die.faces, DieFace(i))

        for (face in die.faces)
            assertEquals(1, die.faceDistribution[face])
    }

    @Test
    fun testStandardD20RNG() {
        val die = D20
        val rng = Random(RNG_SEED)
        assertEquals(1, die.roll(rng).value)
        assertEquals(3, die.roll(rng).value)
        assertEquals(20, die.roll(rng).value)
    }

    @Test
    fun testStandardDP10() {
        val die = DP10
        assertEquals(10, die.faceCount)
        for (i in 0..9)
            assertContains(die.faces, DieFace(10*i))

        for (j in 0..99) {
            val exp = if (j % 10 == 0)  1 else 0
            assertEquals(exp, die.faceDistribution[DieFace(j)])
        }
    }

    @Test
    fun testStandardDP10RNG() {
        val die = DP10
        val rng = Random(RNG_SEED)
        assertEquals(0, die.roll(rng).value)
        assertEquals(20, die.roll(rng).value)
        assertEquals(90, die.roll(rng).value)
    }
}

class TestDicePool {
    @Test
    fun testEmptyPool() {
        val dp = DicePool()

        val rolls = dp.roll(Random(RNG_SEED))
        assertEquals(0, rolls.size)
    }
}
