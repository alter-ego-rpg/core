package alterego_tests

import alterego.model.money.*
import kotlin.test.*

val testGold = Currency("Test Gold", "Gold Currency for testing", 'G')
val testSilver = Currency("Test Silver", "Silver Currency for testing", 'S')
val testCopper = Currency("Test Copper", "Copper Currency for testing", 'C')


class CurrencyTests {

    @Test
    fun test001CurrencyUnit() {
        val goldUnit = testGold.unit
        assertEquals(1.0, goldUnit.value)
    }

    @Test
    fun test002ToString() {
        assertEquals("Test Silver (S)", testSilver.toString())
    }
}

open class MoneyTests {
    protected open val goldUnit = testGold.unit
    protected open val silverUnit = testSilver.unit
    protected open val copperUnit = testCopper.unit

    protected open fun moneyFactory(value: Number, currency: Currency): Money {
        return Money(value, currency)
    }

    @Test
    fun test001EqualsWithIdentity() {
        assertTrue {  copperUnit == copperUnit }
    }

    @Test
    fun test002EqualsWithNew() {
        assertTrue { copperUnit == moneyFactory(1.0, testCopper) }
    }

    @Test
    fun test003NotEqualsDiffValue() {
        assertFalse { copperUnit == moneyFactory(2.0, testCopper) }
    }

    @Test
    fun test004NotEqualsDiffCurrency() {
        assertFalse { copperUnit == moneyFactory(1.0, testSilver) }
    }

    @Test
    fun test005SameHashWithIdentity() {
        assertEquals(copperUnit.hashCode(), copperUnit.hashCode())
    }

    @Test
    fun test006SameHashWithNew() {
        assertEquals(copperUnit.hashCode(), moneyFactory(1.0, testCopper).hashCode())
    }

    @Test
    fun test007DiffHashWithDiffValue() {
        assertFalse { copperUnit == moneyFactory(2.0, testCopper) }
    }

    @Test
    fun test008DiffHashWithDiffCurrency() {
        assertFalse { copperUnit == moneyFactory(1.0, testSilver) }
    }

    @Test
    fun test009MoneyPlusMoneySameCurrency() {
        assertEquals(moneyFactory(4.5, testGold),
               moneyFactory(2.0, testGold) + moneyFactory(2.5, testGold))
    }

    @Test
    fun test010MoneyPlusMoneyDiffCurrency() {
        val ex = assertFailsWith(IncompatibleCurrencyException::class) {
            val res: Money = goldUnit + silverUnit
        }
        assertEquals(testGold, ex.c1)
        assertEquals(testSilver, ex.c2)
    }

    @Test
    fun test011MoneyPlusDouble() {
        assertEquals(moneyFactory(5.5, testGold), moneyFactory(4.0, testGold) + 1.5)
    }

    @Test
    fun test012DoublePlusMoney() {
        assertEquals(moneyFactory(6.0, testGold), 1.0 + moneyFactory( 5.0, testGold))
    }

    @Test
    fun test013MoneyMinusMoneySameCurrency() {
        assertEquals(moneyFactory(6.5, testGold),
               moneyFactory(9.0, testGold) - moneyFactory(2.5, testGold))
    }

    @Test
    fun test014MoneyMinusMoneyDiffCurrency() {
        val ex = assertFailsWith(IncompatibleCurrencyException::class) {
            val res: Money = goldUnit - silverUnit
        }
        assertEquals(testGold, ex.c1)
        assertEquals(testSilver, ex.c2)
    }

    @Test
    fun test015MoneyMinusDouble() {
        assertEquals(moneyFactory(7.5, testGold), moneyFactory(9.0, testGold) - 1.5)
    }

    @Test
    fun test016DoubleMinusMoney() {
        assertEquals(moneyFactory(8.0, testGold), 10.0 - moneyFactory(2.0, testGold))
    }

    @Test
    fun test017MoneyTimesDouble() {
        assertEquals(moneyFactory(8.0, testGold), moneyFactory(2.0, testGold) * 4.0)
    }

    @Test
    fun test018DoubleTimesMoney() {
        assertEquals(moneyFactory(9.0, testGold), 3.0 * moneyFactory(3.0, testGold))
    }

    @Test
    fun test019MoneyDivDouble() {
        assertEquals(moneyFactory(9.5, testGold), moneyFactory(19.0, testGold) / 2.0)
    }

    @Test
    fun test020UnaryMinus() {
        assertEquals(moneyFactory(-10.0, testGold), -moneyFactory(10.0, testGold))
    }

    @Test
    fun test021RangeToMoneySameCurrency() {
        val start = moneyFactory(10.5, testGold)
        val end = Money(21.0, testGold)
        assertEquals(MoneyClosedRange(10.5, 21.0, testGold), start..end)
    }

    @Test
    fun test022RangeFromMoneySameCurrency() {
        val start = Money(11.0, testGold)
        val end = moneyFactory(22.0, testGold)
        assertEquals(MoneyClosedRange(11.0, 22.0, testGold), start..end)
    }

    @Test
    fun test023RangeSameCurrency() {
        val start = moneyFactory(11.5, testGold)
        val end = moneyFactory(23.0, testGold)
        assertEquals(MoneyClosedRange(11.5, 23.0, testGold), start..end)
    }

    @Test
    fun test024RangeToDouble() {
        val start = moneyFactory(12.0, testGold)
        val end = 24.0
        assertEquals(MoneyClosedRange(12.0, 24.0, testGold), start..end)
    }

    @Test
    fun test025RangeFromToDouble() {
        val start = 12.5
        val end = moneyFactory(25.0, testGold)
        assertEquals(MoneyClosedRange(12.5, 25.0, testGold), start..end)
    }

    @Test
    fun test026RangeDiffCurrency() {
        val start = moneyFactory(13.0, testGold)
        val end = moneyFactory(26.0, testSilver)

        val ex = assertFailsWith(IncompatibleCurrencyException::class) {
            val res = start..end
        }
        assertEquals(testGold, ex.c1)
        assertEquals(testSilver, ex.c2)

    }
}

class FlexMoneyTests: MoneyTests() {
    override val goldUnit
        get() = FlexMoney(1.0, testGold)

    override val silverUnit: Money
        get() = FlexMoney(1.0, testSilver)

    override val copperUnit: Money
        get() = FlexMoney(1.0, testCopper)

    override fun moneyFactory(value: Number, currency: Currency): Money {
        return FlexMoney(value, currency)
    }

    @Test
    fun testIsFlexMoney() {
        assertIs<FlexMoney>(goldUnit)
    }

    @Test
    fun test100PlusAssignMoneySameCurrency() {
        val res = goldUnit
        res += Money(9.0, testGold)  // Specifically testing with the root Money class
        assertEquals(moneyFactory(10.0, testGold), res)
    }

    @Test
    fun test101PlusAssignFlexMoneySameCurrency() {
        val res = goldUnit
        res += moneyFactory(9.5, testGold)
        assertEquals(moneyFactory(10.5, testGold), res)
    }

    @Test
    fun test102FlexMoneyPlusAssignDouble() {
        val res = goldUnit
        res += 10.0
        assertEquals(moneyFactory(11.0, testGold), res)
    }

    @Test
    fun test103PlusAssignDiffCurrency() {
        val res = goldUnit
        val ex = assertFailsWith(IncompatibleCurrencyException::class) {
            res += silverUnit
        }
        assertEquals(testGold, ex.c1)
        assertEquals(testSilver, ex.c2)
    }

    @Test
    fun test104MinusAssignMoneySameCurrency() {
        val res = goldUnit
        res -= Money(13.0, testGold)  // Specifically testing with the root Money class
        assertEquals(moneyFactory(-12.0, testGold), res)
    }

    @Test
    fun test105MinusAssignFlexMoneySameCurrency() {
        val res = goldUnit
        res -= moneyFactory(13.5, testGold)
        assertEquals(moneyFactory(-12.5, testGold), res)
    }

    @Test
    fun test106FlexMoneyMinusAssignDouble() {
        val res = goldUnit
        res -= 14.0
        assertEquals(moneyFactory(-13.0, testGold), res)
    }

    @Test
    fun test107MinusAssignDiffCurrency() {
        val res = goldUnit
        val ex = assertFailsWith(IncompatibleCurrencyException::class) {
            res -= silverUnit
        }
        assertEquals(testGold, ex.c1)
        assertEquals(testSilver, ex.c2)
    }

    @Test
    fun test108FlexMoneyTimesAssignDouble() {
        val res = goldUnit
        res *= 14.0
        assertEquals(moneyFactory(14.0, testGold), res)
    }

    @Test
    fun test109FlexMoneyDivAssignDouble() {
        val res = goldUnit
        res /= 25.0
        assertEquals(moneyFactory(0.04, testGold), res)
    }
}
